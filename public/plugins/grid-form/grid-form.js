$.fn.gridForm = function() {    
    $.each(this, function(key, elem) {
        $('.grid-form-input', elem).hide();
        $(elem).click(function() {
            $('.grid-form-label', elem).hide();
            $('.grid-form-input', elem).show();
        });
        $('.grid-form-input input, .grid-form-input select, .grid-form-input textarea', elem).change(function() {
            if ($(this).prop('tagName').toLowerCase() == 'select') {
                var value = $('option:selected', this).text();
            } else {
                var value = $(this).val();
                var format = $(elem).data('format');
                if (format) {
                    switch (format) {
                        case 'number':                   
                            if (!$.isNumeric(value)) {
                                $(this).val(0);
                            }
                            value = locale_number(value);                        
                        break;
                        default:

                        break;
                    }
                }      
            }      
            $('.grid-form-label', elem).html(value);
            
        });
        $(document).bind('click', function(event) {    
            if (elem !== event.target && !elem.contains(event.target)) {                                                        
                $('.grid-form-input', elem).hide();                
                $('.grid-form-label', elem).show();
            }            
        }); 
    });          
}