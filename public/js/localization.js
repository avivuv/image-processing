var localization = {
    date : function(date, format) {
        var timestamp = new Date(date);
        var d = ('00'+(timestamp.getDate()+1)).substr(-2);
        var m = ('00'+(timestamp.getMonth()+1)).substr(-2);
        var Y = timestamp.getFullYear();        
        if (!format) {
            format = lang['date_format'];
        }
        format = format.replace('d', d);
        format = format.replace('m', m);
        format = format.replace('Y', Y);
        return format;
    },
    time : function(date) {
        var timestamp = new Date(date);        
        var H = ('00'+timestamp.getHours()).substr(-2);
        var i = ('00'+timestamp.getMinutes()).substr(-2);
        var s = ('00'+timestamp.getSeconds()).substr(-2);        
        format = lang['time_format'];        
        format = format.replace('H', H);
        format = format.replace('i', i);
        format = format.replace('s', s);
        return format;
    },
    datetime : function(date) {
        var d = ('00'+timestamp.getDate()).substr(-2);
        var d = timestamp.getDate();
        var m = ('00'+(timestamp.getMonth()+1)).substr(-2);
        var Y = timestamp.getFullYear();        
        var H = ('00'+timestamp.getHours()).substr(-2);
        var i = ('00'+timestamp.getMinutes()).substr(-2);
        var s = ('00'+timestamp.getSeconds()).substr(-2);        
        format = lang['datetime_format'];
        format = format.replace('d', d);
        format = format.replace('m', m);
        format = format.replace('Y', Y);
        format = format.replace('H', H);
        format = format.replace('i', i);
        format = format.replace('s', s);
        return format;
    },
    human_date : function(date, format) {                
        var timestamp = new Date(date);
        var d = ('00'+timestamp.getDate()).substr(-2);
        var m = lang['month_'+('00'+(timestamp.getMonth()+1)).substr(-2)];
        var Y = timestamp.getFullYear();      
        var H = ('00'+timestamp.getHours()).substr(-2);
        var i = ('00'+timestamp.getMinutes()).substr(-2);
        var s = ('00'+timestamp.getSeconds()).substr(-2);        
        if (!format) {                        
            format = lang['human_date_format'];                    
        }        
        format = format.replace('H', H);
        format = format.replace('i', i);
        format = format.replace('s', s);
        format = format.replace('d', d);        
        format = format.replace('Y', Y);
        format = format.replace('m', m);        
        return format;
    },
    human_datetime : function(date) {        
        var timestamp = new Date(date);
        var d = ('00'+timestamp.getDate()).substr(-2);
        var m = lang['month_'+('00'+(timestamp.getMonth()+1)).substr(-2)];
        var Y = timestamp.getFullYear();      
        var H = ('00'+timestamp.getHours()).substr(-2);
        var i = ('00'+timestamp.getMinutes()).substr(-2);
        var s = ('00'+timestamp.getSeconds()).substr(-2);        
        format = lang['human_datetime_format'];        
        format = format.replace('H', H);
        format = format.replace('i', i);
        format = format.replace('s', s);
        format = format.replace('d', d);        
        format = format.replace('Y', Y);
        format = format.replace('m', m);
        return format;
    },
    number : function(data) {
        data = parseInt(data);        
        data = $.number(data, 2, lang['decimal_separator'], lang['thousand_separator']);
        return data;
    },
    boolean : function(data, true_result, false_result) {
        if (data == 1 || data == 'true' || data == 't') {        
            if (true_result) {
                return true_result;
            } else {
                return '<i class="fa fa-check text-success"></i>';
            }
        }

        if (data == 0 || data == 'false' || data == 'f') {
            if (true_result) {
                return true_result;
            } else {
                return '<i class="fa fa-times text-danger"></i>';
            }
        }
    }
}