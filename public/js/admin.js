$(function() {
    $('.input-date').datepicker({
        format : 'dd-mm-yyyy'
    });
    $('.grid-form').gridForm();
    $('.select2').select2();
    $('.select2-multiple').select2({
        multiple : true
    });
    $('.input-time').inputmask('99:99');
});

function swalConfirm(msg, action) {
    swal({
      title: msg,
      text: '',
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#dd4b39",
      confirmButtonText: "OK"     
    }, function() {
        if ($.isFunction(action)) {
            action();
        } else {
            document.location.href=action;
        }
    });
}