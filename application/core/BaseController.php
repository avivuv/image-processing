<?php
class BaseController extends CI_Controller {	

    protected $contract = true;    

	public function __construct() {
		parent::__construct();
        $this->load->library('../exceptions/exceptions');
        $this->load->library('../exceptions/model_exceptions');

        $this->load->library(array(
            'session',
            'routes',
            'url_memory',
            'redirect',
            'template',
            'form',
            'response',
            'localization',
            'form_validation'
        ));                

        $this->load->config('middleware');                
        $this->middleware = $this->config->item('middleware');
        $this->load->library('../middleware/auth_middleware');
        $this->load->library('../middleware/authorization_middleware');
        $this->middleware();
        
        $this->contract = $this->contracts();
	}    

	public function middleware() {
		$access['directory'] = trim($this->router->directory, '/');
		$access['class'] = trim($this->router->fetch_class(), '/');
		$access['method'] = trim($this->router->fetch_method(), '/');

        if ($access['directory']) {
            if (isset($this->middleware[$access['directory']]['middleware'])) {                                    
                foreach ($this->middleware[$access['directory']]['middleware'] as $middleware) {                
                    $this->{$middleware.'_middleware'}->handle($this);
                }
            }
        }

        if ($access['class']) {
            if ($access['directory']) {
                if (isset($this->middleware[$access['directory']][$access['class']]['middleware'])) {
                    foreach ($this->middleware[$access['directory']][$access['class']]['middleware'] as $middleware) {
                        $this->{$middleware.'_middleware'}->handle($this); 
                    }
                }
            } else {
                if (isset($this->middleware[$access['class']]['middleware'])) {
                    foreach ($this->middleware[$access['class']]['middleware'] as $middleware) {
                        $this->{$middleware.'_middleware'}->handle($this); 
                    }
                }
            }            
        }        
        if ($access['directory']) {
            if (isset($this->middleware[$access['directory']][$access['class']][$access['method']]['middleware'])) {
                foreach ($this->middleware[$access['directory']][$access['class']][$access['method']]['middleware'] as $middleware) {
                    $this->{$middleware.'_middleware'}->handle($this); 
                }
            }
        } else {
            if (isset($this->middleware[$access['class']][$access['method']]['middleware'])) {
                foreach ($this->middleware[$access['class']][$access['method']]['middleware'] as $middleware) {
                    $this->{$middleware.'_middleware'}->handle($this); 
                }
            }
        }		
	}

	public function contracts() {
		$this->load->library('../contracts/contracts');
		$directory = $this->router->directory;
		$class = $this->router->fetch_class();		
		$method = $this->router->fetch_method();		
		if (file_exists(APPPATH . '/contracts/' . $directory . ucwords($class) . '_contracts.php')) {
			$contracts = strtolower($class . '_contracts');
			$this->load->library('../contracts/' . $directory . $contracts);							
			if (method_exists($this->{$contracts}, $method)) {				
				$params = array_slice($this->uri->rsegment_array(), 2);				
				call_user_func_array(array($this->{$contracts}, $method), $params);				
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
}