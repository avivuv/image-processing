<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends BaseController {

    public function __construct() {
        parent::__construct();
        $this->load->model('image_m');
    }

    public function index() {
        $result = $this->image_m->get();
        $this->load->view('image/index', array(
            'result' => $result
        ));
    }

    public function get() {
        $this->load->library('datatables');
        $this->datatables->resource('images')
            ->generate();
    }

    public function create() {
        $this->load->view('image/create');
    }

    public function store() {
        $upload_config['upload_path'] = './public/img/upload/training/';
        $upload_config['allowed_types'] = 'jpg|jpeg|png';
        $upload_config['max_size']	= '2048';
        $upload_config['encrypt_name'] = TRUE;
        $upload_config['overwrite'] = TRUE;

        $this->upload->initialize($upload_config);

        if (!$this->upload->do_upload('upload_file')){
            $this->redirect->with('errorMessage', $this->upload->display_errors())->back();
        } else {
            $upload_data = $this->upload->data();

            $histogram = array();

            for ($i=0; $i<256; $i++) {
                $histogram['red'][$i] = 0;
                $histogram['green'][$i] = 0;
                $histogram['blue'][$i] = 0;
            }

            if ($upload_data['file_type'] == 'image/jpeg') {
                $im = ImageCreateFromJpeg($upload_data['full_path']);
            } else {
                $im = ImageCreateFromPng($upload_data['full_path']);
            }

            $imgw = imagesx($im);
            $imgh = imagesy($im);

            for ($i=0; $i<$imgw; $i++) {
                for ($j=0; $j<$imgh; $j++) {
                    $rgb = ImageColorAt($im, $i, $j);

                    // extract each value for r, g, b
                    $red = ($rgb >> 16) & 0xFF;
                    $green = ($rgb >> 8) & 0xFF;
                    $blue = $rgb & 0xFF;

                    $histogram['red'][$red] += 1;
                    $histogram['green'][$green] += 1;
                    $histogram['blue'][$blue] += 1;
                }
            }

            $record = array(
                'image_name' => $upload_data['file_name'],
                'file_info' => json_encode($upload_data),
                'histogram' => json_encode($histogram)
            );

            $result = $this->image_m->insert($record);
            if($result) {
               $this->redirect->with('successMessage', 'success_store_image')->to('im_image');
            } else {
               $this->redirect->with('errorMessage', 'failed_store_image')->to('im_image');
            }
        }
    }

    public function histogram($id) {
        $result = $this->image_m->find_or_fail($id);
        $this->load->view('image/histogram', array(
            'result' => $result,
        ));
    }

    public function check_histogram() {
        $result = ImageCreateFromPng('./public/img/upload/ho5WPcQ.png');
        $im = ImageCreateFromJpeg('./public/img/no_image.jpg');

        $imgw = imagesx($result);
        $imgh = imagesy($result);

        for ($i = 0; $i < $imgw; $i++) {
            for ($j = 0; $j < $imgh; $j++) {
                $rgb = ImageColorAt($result, $i, $j);

                // extract each value for r, g, b
                $red = ($rgb >> 16) & 0xFF;
                $green = ($rgb >> 8) & 0xFF;
                $blue = $rgb & 0xFF;


                $val = imagecolorallocate($im, $red, $green, $blue);

                // set the gray value

                imagesetpixel($im, $i, $j, $val);
            }
        }

        header('Content-type: image/jpeg');
        imagejpeg($im);
    }

    public function delete($id) {
        $old = $this->image_m->find_or_fail($id);
        if ($old) {
            unlink(json_decode($old->file_info)->full_path);
        }

        $result = $this->image_m->delete($id);
        if($result) {
            $this->redirect->with('successMessage', 'success_delete_image')->to('im_image');
        } else {
            $this->redirect->with('errorMessage', 'failed_delete_image')->to('im_image');
        }
    }

}