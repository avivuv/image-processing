<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends BaseController {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_m');
    }

    public function login() {
        $this->load->view('login');
    }

    public function authenticate(){
        $post = $this->input->post();
        $userData = $this->db->select('user.*, usergroup.kode_usergroup, usergroup.nama_usergroup')
            ->join('usergroup', 'usergroup.id=user.id_usergroup')
            ->where('username', $post['username'])
            ->where('password', md5($post['password']))
            ->get('user')
            ->row();
        if($userData){
            setLogin(array(
                'id' => $userData->id,
                'nama' => $userData->nama,
                'username' => $userData->username,
                'password' => $userData->password,
                'id_usergroup' => $userData->id_usergroup,
                'kode_usergroup' => $userData->kode_usergroup,
                'nama_usergroup' => $userData->nama_usergroup
            ));
            $this->redirect->with('successMessage', 'success_login_login')->to('dashboard');
        }else{
            $this->redirect->withInput()->with('errorMessage', 'failed_login')->back();
        }
    }

    public function logout(){
        unsetLogin();
        $this->redirect->with('successMessage', 'success_logout')->to('login');
    }

    public function setting(){
        $this->load->view('setting');
    }

    public function update_profile(){
        $post = $this->input->post();
        $result = $this->user_m->update(getLogin('id'), array('nama' => $post['nama']));
        if ($result) {
            setLogin(array(
                'id' => getLogin('id'),
                'nama' => $post['nama'],
                'usernama' => getLogin('username'),
                'password' => getLogin('password'),
                'id_usergroup' => getLogin('id_usergroup'),
                'kode_usergroup' => getLogin('kode_usergroup'),
                'nama_usergroup' => getLogin('nama_usergroup')
            ));
            $this->redirect->with('successMessage', 'success_update_profile')->back();
        } else {
            $this->redirect->with('errorMessage', 'failed_update_profile')->back();
        }
    }

    public function update_password(){
        $post = $this->input->post();
        $result = $this->user_m->update(getLogin('id'), array('password' => md5($post['new_password'])));
        if ($result) {
            setLogin(array(
                'id' => getLogin('id'),
                'nama' => getLogin('nama'),
                'usernama' => getLogin('username'),
                'password' => md5($post['new_password']),
                'id_usergroup' => getLogin('id_usergroup'),
                'kode_usergroup' => getLogin('kode_usergroup'),
                'nama_usergroup' => getLogin('nama_usergroup')
            ));
            $this->redirect->with('successMessage', 'success_update_password')->back();
        } else {
            $this->redirect->with('errorMessage', 'failed_update_password')->back();
        }
    }
}