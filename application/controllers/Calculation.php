<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculation extends BaseController {

    public function __construct() {
        parent::__construct();
        $this->load->model('image_m');
    }

    public function index() {
        $this->load->view('calculation/euclidean_distance');
    }

    public function calculate() {
        $upload_config['upload_path'] = './public/img/upload/testing/';
        $upload_config['allowed_types'] = 'jpg|jpeg|png';
        $upload_config['max_size']	= '2048';
        $upload_config['file_name'] = 'testing_image';
        $upload_config['overwrite'] = TRUE;

        $this->upload->initialize($upload_config);

        if (!$this->upload->do_upload('upload_file')){
            $this->redirect->with('errorMessage', $this->upload->display_errors())->back();
        } else {
            $upload_data = $this->upload->data();

            $testing_histogram = array();

            for ($i = 0; $i < 256; $i++) {
                $testing_histogram['red'][$i] = 0;
                $testing_histogram['green'][$i] = 0;
                $testing_histogram['blue'][$i] = 0;
            }

            if ($upload_data['file_type'] == 'image/jpeg') {
                $im = ImageCreateFromJpeg($upload_data['full_path']);
            } else {
                $im = ImageCreateFromPng($upload_data['full_path']);
            }

            $imgw = imagesx($im);
            $imgh = imagesy($im);

            for ($i = 0; $i < $imgw; $i++) {
                for ($j = 0; $j < $imgh; $j++) {
                    $rgb = ImageColorAt($im, $i, $j);

                    // extract each value for r, g, b
                    $red = ($rgb >> 16) & 0xFF;
                    $green = ($rgb >> 8) & 0xFF;
                    $blue = $rgb & 0xFF;

                    $testing_histogram['red'][$red] += 1;
                    $testing_histogram['green'][$green] += 1;
                    $testing_histogram['blue'][$blue] += 1;
                }
            }

            $data = $this->image_m->get();
            if ($data) {
                foreach ($data as $row) {
                    $red = 0;
                    $green = 0;
                    $blue = 0;

                    for ($i=0; $i<256; $i++) {
                        $red += pow(abs(json_decode($row->histogram)->red[$i] - $testing_histogram['red'][$i]), 2);
                        $green += pow(abs(json_decode($row->histogram)->green[$i] - $testing_histogram['green'][$i]), 2);
                        $blue += pow(abs(json_decode($row->histogram)->blue[$i] - $testing_histogram['blue'][$i]), 2);
                    }

                    $distance = round(sqrt($red) + sqrt($green) + sqrt($blue));

                    $this->image_m->update($row->id, array('distance' => $distance));
                }
            }

            $this->db->order_by('distance')
                ->limit(3);
            $result = $this->image_m->get();

            $this->load->view('calculation/result', array(
                'upload_data' => $upload_data,
                'testing_histogram' => $testing_histogram,
                'result' => $result
            ));
        }
    }
}