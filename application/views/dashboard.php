<?php $this->template->section('content') ?>
<!--    <div class="x_panel">
        <div class="x_content">
			<div class="row">
				<div class="col-md-8">
					<ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
						<li class="active">
							<a href="#grafik_penjualan" data-toggle="tab" aria-expanded="true">
								<span class="hidden-xs">{{perbandingan}}</span>
							</a>
						</li>
						<li class="">
							<a href="#cabang1" data-toggle="tab" aria-expanded="false">
								<span class="hidden-xs">{{cabang_1}}</span>
							</a>
						</li>
						<li class="">
							<a href="#cabang2" data-toggle="tab" aria-expanded="false">
								<span class="hidden-xs">{{cabang_2}}</span>
							</a>
						</li>
						<li class="">
							<a href="#cabang3" data-toggle="tab" aria-expanded="false">
								<span class="hidden-xs">{{cabang_3}}</span>
							</a>
						</li>
						<li class="">
							<a href="#total" data-toggle="tab" aria-expanded="false">
								<span class="hidden-xs">{{total}}</span>
							</a>
						</li>
					</ul>
					<div class="panel">
						<div class="panel-body">
							<div class="tab-content" data-sortable-id="index-3">
								<div class="tab-pane fade active in" id="grafik_penjualan">
									<div width="100%">
										<canvas id="toko_penjualan"></canvas>
									</div>
								</div>
								<div class="tab-pane fade" id="cabang1">
									<div width="100%">
										<canvas id="toko_cabang1"></canvas>
									</div>
								</div>
								<div class="tab-pane fade" id="cabang2">
									<div width="100%">
										<canvas id="toko_cabang2"></canvas>
									</div>
								</div>
								<div class="tab-pane fade" id="cabang3">
									<div width="100%">
										<canvas id="toko_cabang3"></canvas>
									</div>
								</div>
								<div class="tab-pane fade" id="total">
									<div width="100%">
										<canvas id="toko_total"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel">
						<div class="panel-body">
							<p class="text-center">
								<strong>{{belum_dijurnal}}</strong>
							</p>
							<div class="progress-group">
								<span class="progress-text">Penjualan</span>
								<span class="label label-primary pull-right">12</span>
								<div class="progress sm">
									<div class="progress-bar progress-bar-aqua" style="width: 12%"></div>
								</div>
							</div>
							<div class="progress-group">
								<span class="progress-text">Pembelian</span>
								<span class="label label-primary pull-right">15</span>
								<div class="progress sm">
									<div class="progress-bar progress-bar-red" style="width: 15%"></div>
								</div>
							</div>
							<div class="progress-group">
								<span class="progress-text">Hutang</span>
								<span class="label label-primary pull-right">10</span>
								<div class="progress sm">
									<div class="progress-bar progress-bar-green" style="width: 10%"></div>
								</div>
							</div>
							<div class="progress-group">
								<span class="progress-text">Piutang</span>
								<span class="label label-primary pull-right">8</span>
								<div class="progress sm">
									<div class="progress-bar progress-bar-yellow" style="width: 8%"></div>
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<button class="btn btn-success btn-block">{{proses_jurnal}}</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="panel">
						<div class="panel-heading">
							<h4 class="panel-title">{{hutang_jatuh_tempo}}</h4>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>{{tgl_transaksi}}</th>
										<th>{{no_transaksi}}</th>
										<th>{{tgl_jatuh_tempo}}</th>
										<th>{{supplier}}</th>
										<th>{{nominal}}</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>2017-07-16</td>
										<td>20170716</td>
										<td>2017-07-18</td>
										<td>Lorem</td>
										<td class="text-right">1.000.000,00</td>
									</tr>
									<tr>
										<td>2017-07-16</td>
										<td>20170716</td>
										<td>2017-07-19</td>
										<td>Ipsum</td>
										<td class="text-right">750.000,00</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel">
						<div class="panel-heading">
							<h4 class="panel-title">{{piutang_jatuh_tempo}}</h4>
						</div>
						<div class="panel-body">
							<table class="table">
								<thead>
									<tr>
										<th>{{tgl_transaksi}}</th>
										<th>{{no_transaksi}}</th>
										<th>{{tgl_jatuh_tempo}}</th>
										<th>{{customer}}</th>
										<th>{{nominal}}</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>2017-08-16</td>
										<td>20170816</td>
										<td>2017-08-18</td>
										<td>Dolor</td>
										<td class="text-right">1.000.000,00</td>
									</tr>
									<tr>
										<td>2017-08-16</td>
										<td>20170816</td>
										<td>2017-08-19</td>
										<td>Amet</td>
										<td class="text-right">750.000,00</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>-->
	<h1>Selamat Datang</h1>
<?php $this->template->endsection() ?>

<?php /*$this->template->section('page_script') */?><!--
<script>
var config = {
    type: 'line',
    data: {
        labels: ['04/07', '05/07', '06/07', '07/07', '08/07', '09/07','10/07', '11/07', '12/07', '13/07', '14/07', '15/07', '16/07' , '17/07'],
        datasets: [{
            label: "Cabang 1",
            backgroundColor: '#FF5B57',
            borderColor: '#FF5B57',
            data: [100000,125000,520000,412000,430000,206500,55700,170000,225000,125000,434000,430000,521000,155000],
            fill: false,
        }, {
            label: "Cabang 2",
            fill: false,
            backgroundColor: '#348FE2',
            borderColor: '#348FE2',
            data: [170000,225000,125000,434000,430000,521000,155000,100000,125000,520000,412000,430000,206500,55700]
        }, {
            label: "Cabang 3",
            fill: false,
            backgroundColor: '#00A65A',
            borderColor: '#00A65A',
            data: [675000,465000,125300,541000,432000,129000,510000,675000,465000,125300,541000,432000,129000,510000]
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        }
    }
};
var ctx = $("#toko_penjualan").get(0).getContext("2d");
window.myLine = new Chart(ctx, config);

config2 = {
    type: 'line',
    data: {
        labels: ['04/07', '05/07', '06/07', '07/07', '08/07', '09/07','10/07', '11/07', '12/07', '13/07', '14/07', '15/07', '16/07' , '17/07'],
        datasets: [{
            label: "Cabang 1",
            backgroundColor: '#FF5B57',
            borderColor: '#FF5B57',
            data: [100000,125000,520000,412000,430000,206500,55700,170000,225000,125000,434000,430000,521000,155000],
            fill: false,
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        }
    }
};
var ctx2 = $("#toko_cabang1").get(0).getContext("2d");
window.myLine = new Chart(ctx2, config2);

config3 = {
    type: 'line',
    data: {
        labels: ['04/07', '05/07', '06/07', '07/07', '08/07', '09/07','10/07', '11/07', '12/07', '13/07', '14/07', '15/07', '16/07' , '17/07'],
        datasets: [{
           	label: "Cabang 2",
            fill: false,
            backgroundColor: '#348FE2',
            borderColor: '#348FE2',
            data: [170000,225000,125000,434000,430000,521000,155000,100000,125000,520000,412000,430000,206500,55700]
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        }
    }
};
var ctx3 = $("#toko_cabang2").get(0).getContext("2d");
window.myLine = new Chart(ctx3, config3);

config4 = {
    type: 'line',
    data: {
        labels: ['04/07', '05/07', '06/07', '07/07', '08/07', '09/07','10/07', '11/07', '12/07', '13/07', '14/07', '15/07', '16/07' , '17/07'],
        datasets: [{
            label: "Cabang 3",
            fill: false,
            backgroundColor: '#00A65A',
            borderColor: '#00A65A',
            data: [675000,465000,125300,541000,432000,129000,510000,675000,465000,125300,541000,432000,129000,510000]
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        }
    }
};
var ctx4 = $("#toko_cabang3").get(0).getContext("2d");
window.myLine = new Chart(ctx4, config4);

config5 = {
    type: 'line',
    data: {
        labels: ['07/2017', '08/2017', '09/2017', '10/2017'],
        datasets: [{
            label: "Total",
            fill: false,
            backgroundColor: '#000',
            borderColor: '#000',
            data: [5330000, 6400000, 6742000, 7651000]
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        }
    }
};
var ctx5 = $("#toko_total").get(0).getContext("2d");
window.myLine = new Chart(ctx5, config5);
</script>
--><?php /*$this->template->endsection() */?>

<?php $this->template->view('layouts/layout') ?>