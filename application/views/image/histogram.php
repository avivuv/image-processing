<?php $this->template->section('content') ?>
    <h1 class="page-header">
        {{data_gambar}}
        <small>
            <i class="fa fa-angle-right"></i> {{histogram}}
        </small>
        <div class="pull-right">
            <a href="<?= $this->routes->name('im_image') ?>" class="btn btn-default">{{cancel}}</a>
        </div>
    </h1>
    <?php
        $label = array();
        $histogram = array();

        for ($i=0; $i<256; $i++) {
            $label[$i] = $i;
        }

        foreach (json_decode($result->histogram) as $key => $val) {
            $histogram[$key] = $val;
        }
    ?>
    <div class="row">
        <div class="col-md-4">
            <div class="panel">
                <div class="panel-body">
                    <img src="<?= base_url('public/img/upload/training/'.$result->image_name) ?>" style="width: 100%">
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <span>{{file_name}}</span>
                        </div>
                        <div class="col-md-8">
                            <label class="pull-right"><?= ellipsize($result->image_name, 24, .5) ?></label><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <span>{{file_type}}</span>
                        </div>
                        <div class="col-md-8">
                            <label class="pull-right"><?= json_decode($result->file_info)->file_type ?></label><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <span>{{file_size}}</span>
                        </div>
                        <div class="col-md-8">
                            <label class="pull-right"><?= json_decode($result->file_info)->file_size ?></label><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <span>{{image_width}}</span>
                        </div>
                        <div class="col-md-8">
                            <label class="pull-right"><?= json_decode($result->file_info)->image_width ?></label><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <span>{{image_height}}</span>
                        </div>
                        <div class="col-md-8">
                            <label class="pull-right"><?= json_decode($result->file_info)->image_height ?></label><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="active">
                                    <a href="#histogram_rgb" data-toggle="tab" aria-expanded="true">
                                        <span class="hidden-xs">{{RGB}}</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#histogram_red" data-toggle="tab" aria-expanded="false">
                                        <span class="hidden-xs">{{red}}</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#histogram_green" data-toggle="tab" aria-expanded="false">
                                        <span class="hidden-xs">{{green}}</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#histogram_blue" data-toggle="tab" aria-expanded="false">
                                        <span class="hidden-xs">{{blue}}</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="tab-content" data-sortable-id="index-3">
                                        <div class="tab-pane fade active in" id="histogram_rgb">
                                            <div width="100%">
                                                <canvas id="rgb"></canvas>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="histogram_red">
                                            <div width="100%">
                                                <canvas id="red"></canvas>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="histogram_green">
                                            <div width="100%">
                                                <canvas id="green"></canvas>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="histogram_blue">
                                            <div width="100%">
                                                <canvas id="blue"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->template->endsection() ?>

<?php $this->template->section('page_script') ?>
    <script>
        var rgb_config = {
            type: 'bar',
            data: {
                labels: <?= json_encode($label); ?>,
                datasets: [{
                    label: 'Red',
                    fill: false,
                    backgroundColor: '#FF5B57',
                    borderColor: '#FF5B57',
                    data: <?= json_encode($histogram['red']); ?>
                },
                {
                    label: 'Green',
                    fill: false,
                    backgroundColor: '#00A65A',
                    borderColor: '#00A65A',
                    data: <?= json_encode($histogram['green']); ?>
                },
                {
                    label: 'Blue',
                    fill: false,
                    backgroundColor: '#348FE2',
                    borderColor: '#348FE2',
                    data: <?= json_encode($histogram['blue']); ?>
                }]
            },
            options: {
                responsive: true,
                tooltips: {
                    mode: 'index',
                    intersect: false
                }
            }
        };
        var rgb_ctx = $('#rgb').get(0).getContext("2d");
        window.myLine = new Chart(rgb_ctx, rgb_config);

        var red_config = {
            type: 'bar',
            data: {
                labels: <?= json_encode($label); ?>,
                datasets: [{
                    label: 'Red',
                    fill: false,
                    backgroundColor: '#FF5B57',
                    borderColor: '#FF5B57',
                    data: <?= json_encode($histogram['red']); ?>
                }]
            },
            options: {
                responsive: true,
                tooltips: {
                    mode: 'index',
                    intersect: false
                }
            }
        };
        var red_ctx = $('#red').get(0).getContext("2d");
        window.myLine = new Chart(red_ctx, red_config);

        var green_config = {
            type: 'bar',
            data: {
                labels: <?= json_encode($label); ?>,
                datasets: [{
                    label: 'Green',
                    fill: false,
                    backgroundColor: '#00A65A',
                    borderColor: '#00A65A',
                    data: <?= json_encode($histogram['green']); ?>
                }]
            },
            options: {
                responsive: true,
                tooltips: {
                    mode: 'index',
                    intersect: false
                }
            }
        };
        var green_ctx = $('#green').get(0).getContext("2d");
        window.myLine = new Chart(green_ctx, green_config);

        var blue_config = {
            type: 'bar',
            data: {
                labels: <?= json_encode($label); ?>,
                datasets: [{
                    label: 'Blue',
                    fill: false,
                    backgroundColor: '#348FE2',
                    borderColor: '#348FE2',
                    data: <?= json_encode($histogram['blue']); ?>
                }]
            },
            options: {
                responsive: true,
                tooltips: {
                    mode: 'index',
                    intersect: false
                }
            }
        };
        var blue_ctx = $('#blue').get(0).getContext("2d");
        window.myLine = new Chart(blue_ctx, blue_config);
    </script>
<?php $this->template->endsection() ?>

<?php $this->template->getview('layouts/layout') ?>