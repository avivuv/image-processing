<?php $this->template->section('content') ?>
<?php $this->template->getview('layouts/partials/message') ?>
    <h1 class="page-header">
        {{data_gambar}}
        <small>
            <i class="fa fa-angle-right"></i> {{upload_gambar}}
        </small>
        <div class="pull-right">
            <a href="<?= $this->routes->name('im_image') ?>" class="btn btn-default">{{cancel}}</a>
        </div>
    </h1>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->form->openMultipart($this->routes->name('im_image_store'), 'class="form-horizontal"') ?>
            <div class="form-group">
                <label class="control-label col-md-2">{{file_gambar}}</label>
                <div class="col-md-10">
                    <?= $this->form->text('image_file', null, 'class="form-control input-sm" id="image-name"') ?>
                    <input type="file" name="upload_file" id="upload-file" style="display: none;">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">{{preview_gambar}}</label>
                <div class="col-md-10">
                    <img src="" alt="" id="image-preview" class="imagePreview">
                </div>
            </div>
            <button class="btn btn-success pull-right">{{upload}}</button>
            <?= $this->form->close() ?>
        </div>
    </div>
<?php $this->template->endsection() ?>

<?php $this->template->section('page_script') ?>
    <script>
        $(function() {
            $('#image-name').click(function() {
                $('#upload-file').click();
                $('#upload-file').change(function() {
                    var filename = $('#upload-file').val().replace(/^.*\\/, "");
                    $('#image-name').val(filename);
                    readURL(this);
                })
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image-preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
<?php $this->template->endsection() ?>

<?php $this->template->getview('layouts/layout') ?>