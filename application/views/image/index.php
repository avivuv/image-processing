<?php $this->template->section('content') ?>
<?php $this->template->getview('layouts/partials/message') ?>
    <h1 class="page-header">
        {{data_gambar}}
        <div class="pull-right">
            <a href="<?= $this->routes->name('im_image_create') ?>" class="btn btn-primary">{{upload_gambar}}</a>
        </div>
    </h1>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="data-table" class="table table-bordered table-condensed table-row-action">
                <thead>
                <tr>
                    <th>{{nama_gambar}}</th>
                    <th width="150px">{{gambar}}</th>
                    <th width="50px"></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
<?php $this->template->endsection() ?>

<?php $this->template->section('page_script') ?>
    <script>
        $(function() {
            var dataTable = $('#data-table').dataTable({
                processing : true,
                serverSide : true,
                ajax : {
                    url : '<?= $this->routes->name('im_image_get') ?>',
                    type : 'post'
                },
                columns : [
                    {data : 'image_name'},
                    {data : 'file_info', class : 'text-center', render : function (data, type, row) {
                        var data = JSON.parse(data);
                        return '<img src="<?= base_url('public/img/upload/training/'); ?>'+data.file_name+'" class="imagePreviewSm">';
                    }},
                    {data : 'id', class : 'text-center', render : function(data, type, row) {
                        html = '<a href="<?= $this->routes->name('im_image_histogram') ?>/'+data+'" class="btn btn-xs btn-info"><i class="fa fa-bar-chart"></i></a> ';
                        html +=  '<button class="btn btn-xs btn-danger" onclick="swalConfirm(\'{{hapus_gambar}}\',\'<?= $this->routes->name('im_image_delete') ?>/'+data+'\')"><i class="fa fa-trash"></i></button>';
                        return html;
                    }}
                ]
            });
        });
    </script>
<?php $this->template->endsection() ?>

<?php $this->template->getview('layouts/layout') ?>