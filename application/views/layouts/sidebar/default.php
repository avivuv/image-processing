<ul class="nav">
    <li class="nav-header">{{navigation}}</li>
    <li><a href="<?= $this->routes->name('dashboard') ?>"><i class="fa fa-laptop"></i><span>{{dashboard}}</span></a></li>
    <li><a href="<?= $this->routes->name('im_image') ?>"><i class="fa fa-image"></i><span>{{data_gambar}}</span></a></li>
    <li><a href="<?= $this->routes->name('im_calculation') ?>"><i class="fa fa-bar-chart"></i><span>{{perhitungan}}</span></a></li>

    <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
</ul>