<?php if ($error = $this->session->flashdata('error')) { ?>
    <?php if (isset($error['stok'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{periode}}</th>
                <th>{{no_transaksi}}</th>
                <th>{{tanggal_mutasi}}</th>
                <th>{{kode_barang}}</th>
                <th>{{nama_barang}}</th>
                <th>{{satuan}}</th>
                <th>{{stok_barang}}</th>
                <th>{{jumlah_mutasi}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['stok'] as $row) { ?>
                <tr>
                    <td><?= $row['periode'] ?></td>
                    <td><?= $row['no_transaksi'] ?></td>
                    <td><?= $row['tanggal_mutasi'] ?></td>
                    <td><?= $row['kode_barang'] ?></td>
                    <td><?= $row['nama_barang'] ?></td>
                    <td><?= $row['satuan'] ?></td>
                    <td><?= (int)$row['stok_barang'] ?></td>
                    <td><?= (int)$row['jumlah_mutasi'] ?></td>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['stock_opname'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{periode}}</th>
                <th>{{no_transaksi}}</th>
                <th>{{tanggal_mutasi}}</th>
                <th>{{kode_barang}}</th>
                <th>{{nama_barang}}</th>
                <th>{{stok_barang}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['stock_opname'] as $row) { ?>
                <tr>
                    <td><?= $row['periode'] ?></td>
                    <td><?= $row['no_transaksi'] ?></td>
                    <td><?= $row['tanggal_mutasi'] ?></td>
                    <td><?= $row['kode_barang'] ?></td>
                    <td><?= $row['nama_barang'] ?></td>
                    <td><?= (int)$row['stok_barang'] ?></td>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['satuan'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{periode}}</th>
                <th>{{no_transaksi}}</th>
                <th>{{tanggal_mutasi}}</th>
                <th>{{kode_barang}}</th>
                <th>{{nama_barang}}</th>
                <th>{{satuan}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['satuan'] as $row) { ?>
                <tr>
                    <td><?= $row['periode'] ?></td>
                    <td><?= $row['no_transaksi'] ?></td>
                    <td><?= $row['tanggal_mutasi'] ?></td>
                    <td><?= $row['kode_barang'] ?></td>
                    <td><?= $row['nama_barang'] ?></td>
                    <td><?= $row['satuan'] ?></td>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['mapping_barang'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{periode}}</th>
                <th>{{no_transaksi}}</th>
                <th>{{tanggal_mutasi}}</th>
                <th>{{kode_barang}}</th>
                <th>{{nama_barang}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($error['mapping_barang'] as $row) { ?>
                    <tr>
                        <td><?= $row['periode'] ?></td>
                        <td><?= $row['no_transaksi'] ?></td>
                        <td><?= $row['tanggal_mutasi'] ?></td>
                        <td><?= $row['kode_barang'] ?></td>
                        <td><?= $row['nama_barang'] ?></td>
                        <td><?= $row['keterangan'] ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['mapping_supplier'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{kode_supplier}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['mapping_supplier'] as $row) { ?>
                <tr>
                    <td><?= $row['kode_supplier'] ?></td>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['mapping_customer'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{kode_customer}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['mapping_customer'] as $row) { ?>
                <tr>
                    <td><?= $row['kode_customer'] ?></td>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['mapping_kas_bank'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{kode_kas_bank}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['mapping_kas_bank'] as $row) { ?>
                <tr>
                    <td><?= $row['kode_kas_bank'] ?></td>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['mapping_jenis_transaksi'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{kode_jenis_transaksi}}</th>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['mapping_jenis_transaksi'] as $row) { ?>
                <tr>
                    <td><?= $row['kode_jenis_transaksi'] ?></td>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <?php if (isset($error['setting_jurnal'])) { ?>
        <table class="table table-bordered table-condensed table-row-action" width="100%">
            <thead>
            <tr>
                <th>{{keterangan}}</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($error['setting_jurnal'] as $row) { ?>
                <tr>
                    <td><?= $row['keterangan'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>
<?php } ?>