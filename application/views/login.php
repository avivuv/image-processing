<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{sistem_akuntansi}}</title>
    <link href="<?= base_url('public/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/css/theme/default.css') ?>" rel="stylesheet"/>
    <link href="<?= base_url('public/css/style.css') ?>" rel="stylesheet"/>
</head>

<body class="pace-top bg-white">
    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <div id="page-container" class="fade">
        <div class="login login-with-news-feed">
            <div class="news-feed">
                <div class="news-image">
                    <img src="<?= base_url('public/img/login-bg.png') ?>" data-id="login-cover-image" alt="" />
                </div>
                <div class="news-caption">
                    <h4 class="caption-title"><i class="fa fa-money text-success"></i> Sistem Akuntansi</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
            <div class="right-content">
                <div class="login-header">
                    <div class="brand">
                        <span><img src="<?= base_url('public/img/logo.png') ?>" width="75px"></span> {{sistem_akuntansi}}
                    </div>
                    <div class="icon">
                        <i class="fa fa-sign-in"></i>
                    </div>
                </div>
                <div class="login-content">
                    <?php $this->template->getview('layouts/partials/message') ?>
                    <?php $this->template->getview('layouts/partials/validation') ?>
                    <form action="<?= $this->routes->name('authenticate') ?>" method="POST" class="margin-bottom-0">
                        <div class="form-group has-feedback">
                            <input type="text" name="username" class="form-control input-lg" placeholder="Username" required />
                            <span class="form-control-feedback"><i class="fa fa-user"></i></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="password" class="form-control input-lg" placeholder="Password" required />
                            <span class="form-control-feedback"><i class="fa fa-lock"></i></span>
                        </div>
                        <div class="login-buttons">
                            <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                        </div>
                       
                        <hr />
                        <p class="text-center">
                            &copy; Sitem Akuntansi All Right Reserved 2017
                        </p>
                    </form>
                </div>
            </div>
        </div>      
    </div>
    <script src="<?= base_url('public/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('public/plugins/jquery-ui/ui/minified/jquery-ui.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jquery-cookie/jquery.cookie.js') ?>"></script>
    <script src="<?= base_url('public/js/apps.min.js') ?>"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
</body>
</html>