<?php $this->template->section('content') ?>
    <h1 class="page-header">
        {{pengaturan}}
    </h1>

    <div class="box box-danger">
        <div class="box-body box-spacing">
            <?= $this->template->view('layouts/partials/message'); ?>
            <?= $this->template->view('layouts/partials/validation'); ?>

            <?= $this->form->model(getLogin(), $this->routes->name('update_profile'), null, null, false) ?>
            <div class="row">
                <div class="col-md-2">
                    <h4 class="form-title">{{data_profil}}</h4>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <label>{{nama}}</label>
                        <?= $this->form->text('nama', null, 'class="form-control input-sm"') ?>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success"><i class="fa fa-check"></i> {{update_profil}}</button>
                    </div>
                </div>
            </div>
            <?= $this->form->close() ?>

            <hr>
            <?= $this->form->open($this->routes->name('update_password'), null, null, false) ?>
            <div class="row">
                <div class="col-md-2">
                    <h4 class="form-title">{{ubah_password}}</h4>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <label>{{password_lama}}</label>
                        <?= $this->form->password('old_password', null, 'class="form-control input-sm"') ?>
                    </div>
                    <div class="form-group">
                        <label>{{password_baru}}</label>
                        <?= $this->form->password('new_password', null, 'class="form-control input-sm"') ?>
                    </div>
                    <div class="form-group">
                        <label>{{konfirmasi_password_baru}}</label>
                        <?= $this->form->password('confirm_new_password', null, 'class="form-control input-sm"') ?>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success"><i class="fa fa-key"></i> {{change_password}}</button>
                    </div>
                </div>
            </div>
            <?= $this->form->close() ?>
        </div>
    </div>
<?php $this->template->endsection() ?>

<?php $this->template->view('layouts/layout') ?>