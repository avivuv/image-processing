<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('REPORT_PATH', FCPATH . 'other/reports/');
define('REPORT_RESOURCES_PATH', REPORT_PATH . 'libs/');
define('OUTPUT_REPORT_PATH', REPORT_PATH . 'output/');
define('REPORT_EXT', '.jrxml');
define('COMPILED_REPORT_EXT', '.jasper');
define('OUTPUT_REPORT_TYPE', 'pdf');
define('OUTPUT_REPORT_EXT', '.pdf');

function generate_report($jasper, $reportName, $reportSuffix, $reportParam = [], $reportConnection = []) {
	$reportSource = REPORT_PATH . $reportName . COMPILED_REPORT_EXT;
	$targetReport = OUTPUT_REPORT_PATH . $reportName . '_' . $reportSuffix;

	// Process a Jasper file to PDF and RTF (you can use directly the .jrxml)
	$output = $jasper->process ( $reportSource, $targetReport, array (
		OUTPUT_REPORT_TYPE
	), $reportParam, $reportConnection, false )->execute (); // output();var_dump($output);die;

	return ["{$reportName}_{$reportSuffix}" . OUTPUT_REPORT_EXT, $targetReport . OUTPUT_REPORT_EXT];
}