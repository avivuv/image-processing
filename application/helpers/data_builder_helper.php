<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function lists($data, $id, $label, $placeholder = null, $placeholder_value = '') {
    $CI = &get_instance();
    $lists = array();
    if ($placeholder) {
        $lists[$placeholder_value] = $CI->localization->get($placeholder);
    }
    foreach ($data as $row) {
        $row = (Object) $row;
        $lists[$row->$id] = $row->$label;
    }
    return $lists;
}

function tree_data($data, $id, $parent, $start = '')   {	
    $tree = array();
    $result = array();
        foreach ($data as $row) {
            $tree[$row->$parent][] = (Object) $row;
        }
        if (isset($tree[$start])) {
        $result = set_tree_data($tree, $tree[$start], $id, $parent);
    }
    return $result;
}

function tree_data_lists($data, $id, $parent, $start = '', $value, $label = array(), $placeholder = null, $placeholder_value = '') {
    $tree = tree_data($data, $id, $parent, $start);        
    $lists = array();
    if ($placeholder) {
        $lists[$placeholder_value] = $placeholder;
    }
    foreach ($tree as $row) {
        $row = (Object) $row;
        $lists[$row->$value] = str_repeat('<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>', $row->tree_level) . $row->$label[0] . ' - ' . $row->$label[1];
    }
    return $lists;
}

function tree_data_list_json($data, $id, $parent, $start = '', $value, $label = array(), $placeholder = null, $placeholder_value = ''){
    $data = tree_data_lists($data, $id, $parent, $start = '', $value, $label, $placeholder, $placeholder_value);
    //$result[] = array('value' => '', 'text' => '');
    foreach ($data as $key => $value) {
        $result[] = array('value' => $key, 'text' => $value);
    }
    return $result;
}

function set_tree_data($data, $parent_data, $id, $parent, $level = 0) {
    $result = array();  
    foreach ($parent_data as $key => $row) {
        $row->tree_level = $level;
        $result[] = $row;				
        if (isset($data[$row->$id])) {				
            $result = array_merge($result, set_tree_data($data, $data[$row->$id], $id, $parent_data, $level + 1));
            unset($data[$row->$id]);
        }
    }	
    return $result;    
}

function autocomplete($data, $id, $label, $value) {
    $lists = array();
    foreach ($data as $row) {
        $lists[] = array(
            'id' => $row->$id,
            'label' => $row->$label,
            'value' => $row->$value,
            'data' => $row
        );
    }
    return $lists;
}

function jurnal_memorial_lists(){
    $list = array();
    $list[''] = '';
    $list['1'] = 'Sekali';
    $list['2'] = 'Berkala';
    return $list;
}

function day_lists(){
    $list = array();
    for ($i=1; $i <= 7 ; $i++) { 
        $list[$i] = '{{day_'.$i.'}}';
    }
    return $list;
}