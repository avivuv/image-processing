<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	function sum_number($data, $index, $currency = ''){
		$result = 0;
		foreach ($data as $row) {
			$result += $row->$index;
		}
		if($currency) {
			$CI = &get_instance();
			$result = $CI->localization->currency($result);
		}
		return $result;
	}