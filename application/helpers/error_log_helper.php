<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function setError($data) {
    $CI = &get_instance();
    if ($session = $CI->session->userdata('error_log')) {
        if (isset($session[$key])) {
            $CI->session->set_userdata('error_log', array_merge($session, $data));
        } else {
            $CI->session->set_userdata('error_log', array($key => $data));
        }
    }else {
        $CI->session->set_userdata('error_log', $data);
    }
}

function getError($key = null) {
    $CI = &get_instance();
    if ($session = $CI->session->userdata('error_log')) {
        if ($key) {
            if (isset($session[$key])) {
                return $session[$key];
            } else {
                return null;
            }
        } else {
            return $session;
        }
    }
}

function unsetError() {
    $CI = &get_instance();
    $CI->session->unset_userdata('error_log');
}