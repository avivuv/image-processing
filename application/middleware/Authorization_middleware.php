<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authorization_middleware {

    public function handle($middleware) {
        $directory = trim($middleware->router->directory, '/');
        $class = trim($middleware->router->fetch_class());
        $method = trim($middleware->router->fetch_method());

        if ($directory) {
            $middleware->db->where('directory', $directory);
        }

        $result = $middleware->db->where('class', $class)
            ->where('method', $method)
            ->where('permission', 0)
            ->where('id_usergroup', getLogin('id_usergroup'))
            ->get('usergroup_permission')
            ->row();

        if ($result) {
            $middleware->redirect->with('errorMessage', 'error_permission_denied')->back();
        }
    }

}