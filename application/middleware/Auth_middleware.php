<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_middleware {

    public function handle($middleware) {
        if (!isLogin()) {
            $middleware->redirect->guest('login');
        }
    }

}