<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usergroup_m extends BaseModel {

    protected $table = 'usergroup';
    protected $fillable = array('kode_usergroup', 'nama_usergroup');
    protected $author = true;
    protected $timestamp = true;

    public function insert($record) {
        $record['kode_usergroup'] = str_replace(' ', '_', strtolower($record['nama_usergroup']));
        return parent::insert($record);
    }

    public function update($id, $record) {
        $record['kode_usergroup'] = str_replace(' ', '_', strtolower($record['nama_usergroup']));
        return parent::update($id, $record);
    }
}