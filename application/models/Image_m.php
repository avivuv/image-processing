<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_m extends BaseModel {

    protected $table = 'images';
    protected $fillable = array('image_code', 'image_name', 'file_info', 'histogram', 'distance');
    protected $timestamp = true;

}