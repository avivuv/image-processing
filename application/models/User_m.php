<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends BaseModel {

    protected $table = 'user';
    protected $fillable = array('id_usergroup', 'nama', 'username', 'password');
    protected $order_by = 'nama';
    protected $order = 'asc';
    protected $author = true;
    protected $timestamp = true;

    public function insert($record) {
        $record['password'] = md5($record['password']);
        return parent::insert($record);
    }
}