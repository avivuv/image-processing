<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatables {

    protected $CI;  

    private $resource;

    private $select = '*';

    private $join = array();

    private $where = array();

    protected $add_columns = array();

    protected $edit_columns = array();

    protected $database;

    protected $group_by;

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function database($db) {
        $this->database = $db;
        return $this;
    }

    public function resource($table, $select = '*') {
        $this->resource = $table;
        $this->select = $select;
        return $this;
    }

    public function join($table, $cond, $type = '', $escape = null) {
        $this->join[] = array(
            'table' => $table,
            'cond' => $cond,
            'type' => $type,
            'escape' => $escape
        );
        return $this;
    }

    public function where($key, $value = null, $escape = null) {
        $this->where[] = array(
            'type' => 'where',
            'key' => $key,
            'value' => $value,
            'escape' => $escape
        );
        return $this;
    }

    public function where_in($key, $value = null, $escape = null) {
        $this->where[] = array(
            'type' => 'where_in',
            'key' => $key,
            'value' => $value,
            'escape' => $escape
        );
        return $this;
    }

    public function or_where($key, $value = null, $escape = null) {
        $this->where[] = array(
            'type' => 'or_where',
            'key' => $key,
            'value' => $value,
            'escape' => $escape
        );
        return $this;
    }

    public function or_where_in($key, $value = null, $escape = null) {
        $this->where[] = array(
            'type' => 'or_where_in',
            'key' => $key,
            'value' => $value,
            'escape' => $escape
        );
        return $this;
    }    

    public function like($key, $value = null, $side = 'both', $escape = null) {
        $this->where[] = array(
            'type' => 'like',
            'key' => $key,
            'value' => $value,
            'side' => $side,
            'escape' => $escape
        );
        return $this;
    }

    public function or_like($key, $value = null, $side = 'both', $escape = null) {
        $this->where[] = array(
            'type' => 'or_like',
            'key' => $key,
            'value' => $value,
            'side' => $side,
            'escape' => $escape
        );
        return $this;
    }

    public function group_by($field){
        $this->group_by = $field;
        return $this;
    }

    public function add_column($name, $value) {
        $this->add_columns[$name] = $value;
        return $this;
    }

    public function edit_column($name, $value) {
        $this->edit_columns[$name] = $value;
        return $this;
    }

    private function build_join() {
        foreach ($this->join as $join) {
            $this->CI->db->join($join['table'], $join['cond'], $join['type'], $join['escape']);
        }        
    }

    private function build_where() {
        if (count($this->where) <> 0) {
            $this->CI->db->group_start();
        }
        foreach ($this->where as $where) {
            switch ($where['type']) {
                case 'where':
                    $this->CI->db->where($where['key'], $where['value'], $where['escape']);
                    break;
                case 'where_in':
                    $this->CI->db->where_in($where['key'], $where['value'], $where['escape']);
                    break;
                case 'or_where':
                    $this->CI->db->or_where($where['key'], $where['value'], $where['escape']);
                    break;                    
                case 'or_where_in':
                    $this->CI->db->or_where_in($where['key'], $where['value'], $where['escape']);
                    break;  
                case 'like':
                    $this->CI->db->like($where['key'], $where['value'], $where['sode'], $where['escape']);
                    break;                    
                case 'or_like':
                    $this->CI->db->or_like($where['key'], $where['value'], $where['sode'], $where['escape']);
                    break;                    
            }            
        }
        if (count($this->where) <> 0) {
            $this->CI->db->group_end();
        }   
    } 

    public function generate($return = false) {
        $params = $this->CI->input->post();
        if($this->database) {
            $this->CI->db = $this->CI->load->database($this->CI->connection->load($this->database), true);
        }
        $this->CI->db->select($this->select);
        if($this->group_by){
            $this->CI->db->group_by($this->group_by);
        }
        $this->build_join();
        $this->build_where();
        $this->CI->db->limit($params['length'], $params['start']);
        foreach ($params['order'] as $order) {
            $this->CI->db->order_by($params['columns'][$order['column']]['data'], $order['dir']);   
        }
        if ($params['search']['value']) {
            foreach ($params['columns'] as $column) {
                if ($column['searchable'] == 'true') {
                    $searchable[] = $column['data'];
                }
            }
            if (count($searchable) <> 0) {
                $this->CI->db->group_start();
                if ($this->CI->db->platform() == 'oci8') {           
                    $this->CI->db->like('lower("'.$searchable[0].'")', strtolower($params['search']['value']));
                } else {
                    $this->CI->db->like($searchable[0], $params['search']['value']);
                }
                unset($searchable[0]);
                foreach ($searchable as $column) {
                    $this->CI->db->or_like($column, $params['search']['value']);
                }
                $this->CI->db->group_end();
            }
        }        
        $result = $this->CI->db->get($this->resource);        
        $data = $result->result();
        if (count($this->add_columns) <> 0) {
            foreach ($data as $key => $row) {
                foreach ($this->add_columns as $add_column => $value) {
                    if (is_callable($value)) {
                        $row->$add_column = $value($row);
                    } else {
                        $row->$add_column = $value;
                    }
                    $data[$key] = $row;
                }
            }            
        }

        if (count($this->edit_columns) <> 0) {
            foreach ($data as $key => $row) {
                foreach ($this->edit_columns as $edit_column => $value) {
                    if (is_callable($value)) {
                        $row->$edit_column = $value($row);
                    } else {
                        $row->$edit_column = $value;
                    }
                    $data[$key] = $row;
                }
            }            
        }        

        $this->build_join();
        $this->build_where();
        if($this->group_by) {
            $this->CI->db->select($this->select);
            $this->CI->db->group_by($this->group_by);
        }
        $record_total = $this->CI->db->count_all_results($this->resource);

        $response = array(
            'draw' => $params['draw'],
            'recordsTotal' => $record_total,
            'recordsFiltered' => $record_total,
            'data' => $data
        );    
        if ($return) {
            return $response;
        } else {
            $this->CI->output->set_content_type('application/json')->set_output(json_encode($response));
        }
    }

}