<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Connection {

    protected $CI;

    public function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->library('session');

        if (!$this->CI->session->userdata('database')) {
            $database_list = $this->CI->db->get('setting_toko')->result();

            $database = array();
            foreach ($database_list as $db) {
                $database[$db->id] = array(
                    'dsn'	=> '',
                    'hostname' => $db->hostname,
                    'username' => $db->username,
                    'password' => $db->password,
                    'database' => $db->database,
                    'dbdriver' => $db->dbdriver,
                    'dbprefix' => $db->dbprefix,
                    'pconnect' => $db->pconnect,
                    'db_debug' => $db->db_debug,
                    'cache_on' => $db->cache_on,
                    'cachedir' => $db->cachedir,
                    'char_set' => $db->char_set,
                    'dbcollat' => $db->dbcollat,
                    'swap_pre' => '',
                    'encrypt' => FALSE,
                    'compress' => FALSE,
                    'stricton' => FALSE,
                    'failover' => array(),
                    'save_queries' => TRUE
                );
            }
            $this->CI->session->set_userdata('database', $database);
        }
    }

    function load($key) {
        if ($session = $this->CI->session->userdata('database')) {
            if ($key) {
                if (isset($session[$key])) {
                    return $session[$key];
                } else {
                    return null;
                }
            } else {
                return $session;
            }
        }
    }
}