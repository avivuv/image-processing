<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseForm_validation extends CI_Form_validation {

    public function set_rules($name, $label = '', $rules = array(), $errors = array()) {
        parent::set_rules($name, $this->CI->localization->get($label), $rules, $errors);
    }

    public function matches($str, $field)
    {
        if (is_numeric($this->CI->localization->number_value($str)) && $this->CI->localization->number_value($this->CI->input->post($field))) {
            if ($this->CI->localization->number_value($str) != $this->CI->localization->number_value($this->CI->input->post($field))) {
                return false;
            } else {
                return true;
            }
        } else {
            return parent::matches($str, $field);
        }
    }

}