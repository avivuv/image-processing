<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_exceptions extends Exceptions {

	public function __construct() {		
		parent::__construct();		
	}

	public function find_or_fail() {
		$this->exceptions->redirect->with('errorMessage', $this->get_message())->back();
	}
    
}
