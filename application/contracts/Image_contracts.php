<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_contracts extends Contracts {

    public function store() {
        $this->contracts->load->library('form_validation');
        $this->contracts->form_validation->set_rules('image_file', 'File Gambar', 'required');

        if (!$this->contracts->form_validation->run()) {
            $this->contracts->redirect->withInput()->withValidation()->back();
        }
    }
}
