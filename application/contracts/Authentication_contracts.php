<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication_contracts extends Contracts {

    public function update_profile() {
        $this->contracts->load->library('form_validation');
        $this->contracts->form_validation->set_rules('nama', 'nama', 'required');
        if (!$this->contracts->form_validation->run()) {
            $this->contracts->redirect->withInput()->withValidation()->back();
        }
    }

    public function update_password() {
        $this->contracts->load->library('form_validation');
        $post = $this->contracts->input->post();
        if (md5($post['old_password']) != getLogin('password')) {
            $this->contracts->form_validation->set_rules('old_password', 'old_password', 'required|old_password_check', array(
                'old_password_check' => 'The {field} does not match.'
            ));
        }
        $this->contracts->form_validation->set_rules('new_password', 'new_password', 'required');
        $this->contracts->form_validation->set_rules('confirm_new_password', 'confirm_new_password', 'required|matches[new_password]');
        if (!$this->contracts->form_validation->run()) {
            $this->contracts->redirect->withInput()->withValidation()->back();
        }
    }

}
