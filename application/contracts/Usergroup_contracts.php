<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usergroup_contracts extends Contracts {

    public function store() {
        $this->validation();
    }

    public function update() {
        $this->validation();
    }

    private function validation() {
        $this->contracts->load->library('form_validation');
        $this->contracts->form_validation->set_rules('nama_usergroup', 'nama_group', 'required');
        if (!$this->contracts->form_validation->run()) {
            $this->contracts->redirect->withInput()->withValidation()->back();
        }
    }

}
