<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_contracts extends Contracts {

    public function store() {
        $this->contracts->load->library('form_validation');
        $this->contracts->form_validation->set_rules('nama', 'nama_pengguna', 'required');
        $this->contracts->form_validation->set_rules('username', 'username', 'required|is_unique[user.username]');
        $this->contracts->form_validation->set_rules('password', 'password', 'required');
        $this->contracts->form_validation->set_rules('confirm_password', 'konfirmasi_password', 'required|matches[password]');
        if (!$this->contracts->form_validation->run()) {
            $this->contracts->redirect->withInput()->withValidation()->back();
        }
    }

    public function update() {
        $this->contracts->load->library('form_validation');
        $this->contracts->form_validation->set_rules('nama', 'nama_pengguna', 'required');
        if (!$this->contracts->form_validation->run()) {
            $this->contracts->redirect->withInput()->withValidation()->back();
        }
    }

}
